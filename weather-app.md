# Weather App

This is a simple app developed with [**React**](https://reactjs.org/), [**React hooks**](https://reactjs.org/docs/hooks-intro.html) for state management using browser integrated [**fetch api**](https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API) to gather data to feed this application.

There are a few examples for unit tests developed with [**testing library**](https://testing-library.com/docs/react-testing-library/intro/) and an end to end test with [**cypress**](https://www.cypress.io/)


## Requirements

This application needs one of the following requirements to run:
 - node version 12 or higher
 - docker compose

## How to run it

We can run this application by 2 different ways:
 1 - locally by running:
    `npm install`
    `npm start`
 2 - inside a docker container running:
    `docker-compose up`

### What the app does

This simple app is split into 2 parts, the 1st one is simply a request to open weather api based on client position coordinates (with a default value when there is no access to client position) and the 2nd one with a search by the city where is been done a request to obtain that city coordinates and another one to obtain the weather forecast.
This last part has some logic around multiple forecasts representation, their duplication, and remotion.

### Additional instructions to check

We can run `npm run test` to check the existing unit tests result and `npm run e2e` to check the result of the existing end-to-end test.