type Weather = {
    description: string;
    icon: string;
};

export type WeatherData = {
    dt: number,
    weather: Array<Weather>,
    temp: {
        min: number;
        max: number;
    }
};

export type WeatherDisplayProps = {
    weatherData: [] | Array<WeatherData>,
    timezone: string;
    displayName: string;
};
