import { WeatherDisplayProps, WeatherData } from './index.d';
import './weather-display.css';

export const buildDate = (timezone, dayToAdd = 0) => {
    const date = new Date();

    date.setDate(date.getDate() + dayToAdd);
    const parsedDate = date.toLocaleString("pt-PT", {timeZone: timezone}).split(',')[0];

    return parsedDate;
}

const WeatherDisplay = ({ weatherData, timezone, displayName }: WeatherDisplayProps) => {
    let forecastWeather = [];
    const logoUrl = (logoId: string) => `http://openweathermap.org/img/wn/${logoId}@2x.png`;

    if(weatherData.length === 0) {
        return null;
    }

    const weatherItem = (item: WeatherData, index = 0) => {
        return (
            <div
                data-testid="weather-item"
                className="WeatherDisplay-weatherItem"
                key={`daily_weather_${item.dt}_${index}`}>
                    {buildDate(timezone, index)}
                    <img
                        data-testid={`weather-item-icon-${item.weather[0].icon}`}
                        className="WeatherDisplay-icon"
                        alt={item.weather[0].description}
                        src={logoUrl(item.weather[0].icon)} />
                    <span>{item.weather[0].description}</span>
                    <span>Min {item.temp.min}ºC</span>
                    <span>Max {item.temp.max}ºC</span>
            </div>
        );
    }

    weatherData.forEach((dailyWeather, index) => {
        if (index > 0) {
            forecastWeather.push(weatherItem(dailyWeather, index));
        }
    });

    return (
        <div className="WeatherDisplay" data-testid="weather-display">
            <h1>{displayName}</h1>
            <h2>Current Weather</h2>
            <div className="WeatherDisplay-today">
                {weatherItem(weatherData[0])}
            </div>
            <h2>Weather Forecast</h2>
            <div className="WatherDisplay-forecast">
                { forecastWeather }
            </div>
        </div>
    );
}

export default WeatherDisplay;
