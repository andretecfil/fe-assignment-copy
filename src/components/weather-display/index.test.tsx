import { render, screen } from '@testing-library/react';
import WeatherDisplay, {buildDate} from './index';

describe('WeatherDisplay', () => {
    const MOCK_PROPS = {
        weatherData: [
            {
                dt: 1618317040,
                weather: [
                    {
                        description: 'windy',
                        icon: '10d',
                    }
                ],
                temp: {
                    min: 12.1,
                    max: 14.2
                }
            },
            {
                dt: 1618317040,
                weather: [
                    {
                        description: 'sunny',
                        icon: '11d',
                    }
                ],
                temp: {
                    min: 14.1,
                    max: 17.2
                }
            }
        ],
        timezone: 'America/Los_Angeles',
        displayName: 'Test location',
    };

    test('should renders correctly', () => {
        render(<WeatherDisplay {...MOCK_PROPS} />);

        expect(screen.getByText('Current Weather')).toBeInTheDocument();
        expect(screen.getByText('Weather Forecast')).toBeInTheDocument();
        expect(screen.getByText(MOCK_PROPS.displayName)).toBeInTheDocument();
        expect(screen.queryAllByTestId('weather-item').length).toBe(MOCK_PROPS.weatherData.length);
        MOCK_PROPS.weatherData.forEach((item) => {
            expect(screen.getByText(`Min ${item.temp.min}ºC`)).toBeInTheDocument();
            expect(screen.getByText(`Max ${item.temp.max}ºC`)).toBeInTheDocument();
            expect(screen.getByText(item.weather[0].description)).toBeInTheDocument();
            expect(screen.getByTestId(`weather-item-icon-${item.weather[0].icon}`)).toBeInTheDocument();
        });
    });

    test('should not render when there is no weatherData to show', () => {
        render(<WeatherDisplay {...MOCK_PROPS } weatherData={[]} />);

        expect(screen.queryByText('Current Weather')).not.toBeInTheDocument();
        expect(screen.queryByText('Weather Forecast')).not.toBeInTheDocument();
        expect(screen.queryByText(MOCK_PROPS.displayName)).not.toBeInTheDocument();
        expect(screen.queryAllByTestId('weather-item').length).toBe(0);
    });

    describe('buildDate', () => {
        test('should return the expected formatted dates', () => {
            const timezone = 'America/Los_Angeles';
            const numberOfChecks = 7;

            for(let i = 0; i < numberOfChecks; i++) {
                const date = new Date();
                date.setDate(date.getDate() + i);
                const expectedDate = date.toLocaleString("pt-PT", {timeZone: timezone}).split(',')[0];
    
                expect(buildDate(timezone, i)).toEqual(expectedDate);
            }
        });
    });
});
