import { useState } from 'react';
import { useForecastWeatherDataFromCoords, useReverseGeolocationData } from '../../hooks';
import { Location, ForecastWeather } from '../../hooks/index.d';
import { getLocation } from '../../utils';
import WeatherDisplay from '../weather-display';
import '../../App.css';

function AssignmentPart1() {
  const [location] = useState(getLocation());
  const [reverseGeolocationData] = useReverseGeolocationData(location);
  const [forecastWeatherData] = useForecastWeatherDataFromCoords(location);
  location as Location;
  reverseGeolocationData as string;
  forecastWeatherData as ForecastWeather;

  if (!(location.lat && location.long) || reverseGeolocationData.length === 0 || forecastWeatherData.daily?.length === 0) {
    return null;
  }

  return (
    <div data-testid="assignement-part-1">
      <header className="App-header"> Part 1 </header>
      <WeatherDisplay
        weatherData={forecastWeatherData.daily ? forecastWeatherData.daily : []}
        timezone={forecastWeatherData.timezone}
        displayName={reverseGeolocationData} />
    </div>
  );
}

export default AssignmentPart1;
