import { render, screen } from '@testing-library/react';
import AssignmentPart1 from './index';

const mockUseReverseGeolocationData = jest.fn();
const mockUseForecastWeatherDataFromCoords = jest.fn();

jest.mock('../../hooks', () => ({
    useReverseGeolocationData: () => mockUseReverseGeolocationData(),
    useForecastWeatherDataFromCoords: () => mockUseForecastWeatherDataFromCoords()
}));

describe.skip('AssignmentPart1', () => {
    test('should contain WeatherDisplay when there is weather data', () => {
        mockUseReverseGeolocationData.mockImplementation(() => ["Coimbra"]);
        mockUseForecastWeatherDataFromCoords.mockImplementation(() => {
            return {
                daily: [
                    {
                        dt: 1618317040,
                        weather: [
                            {
                                description: 'windy',
                                icon: '10d',
                            }
                        ],
                        temp: {
                            min: 12.1,
                            max: 14.2
                        }
                    },
                    {
                        dt: 1618317040,
                        weather: [
                            {
                                description: 'sunny',
                                icon: '11d',
                            }
                        ],
                        temp: {
                            min: 14.1,
                            max: 17.2
                        }
                    }
                ],
                timezone: 'America/Los_Angeles',
            }
        });
        render(<AssignmentPart1 />);

        expect(screen.getByText('Part 1')).toBeInTheDocument();
        expect(screen.getByTestId('weather-display')).toBeInTheDocument();
    });

    test('should contains WeatherDisplay when there is weather data', () => {
        mockUseReverseGeolocationData.mockImplementation(() => ["Coimbra"]);
        mockUseForecastWeatherDataFromCoords.mockImplementation(() => {
            return {
                daily: [],
                timezone: 'America/Los_Angeles',
            }
        });
        render(<AssignmentPart1 />);

        expect(screen.getByText('Part 1')).toBeInTheDocument();
        expect(screen.queryByTestId('weather-display')).not.toBeInTheDocument();
    });
});
