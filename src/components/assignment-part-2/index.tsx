import { useState, useEffect } from 'react';
import { useForecastWeatherDataFromCityName } from '../../hooks';
import { ForecastWeather } from '../../hooks/index.d';
import WeatherDisplay from '../weather-display';
import './index.css';

function AssignmentPart2() {
  const [weatherData, getWeatherData] = useForecastWeatherDataFromCityName("");
  const [city, setCity] = useState("");
  const [listOfCitiesData, setListOfCitiesData] = useState([]);
  weatherData as ForecastWeather;
  city as string;
  listOfCitiesData as ForecastWeather[];

  console.log(weatherData)

  useEffect(() => {
    // check if there is weather data to add to our list
    if (Object.keys(weatherData).length > 0) {
      setListOfCitiesData([weatherData, ...listOfCitiesData]);
    }
  }, [weatherData]);

  const onClick = () => {
    // check if there is data for a given city already
    let existingCity = false;
    listOfCitiesData.forEach(({ name }) => {
      if (city === name) {
        existingCity = true;
        return;
      }
    });

    if (!existingCity) {
      getWeatherData(city);
    } else {
      window.alert("There is a weather forecast for that city already");
    }

    setCity("");
  }

  const onChange = (event) => {
    setCity(event.target.value);
  }

  const onDeleteClick = (indexToDelete) => {
    const listOfCities = [...listOfCitiesData];
    listOfCities.splice(indexToDelete, 1)
    setListOfCitiesData(listOfCities);
  }

  console.log(listOfCitiesData)

  return (
    <div data-testid="assignement-part-2">
      <header className="header"> Part 2 </header>
      <div className="search-wrapper" data-testid="city-search">
        <input
          className="search-input"
          placeholder="Location to search"
          value={city}
          onChange={onChange}></input>
        <button className="search-button" onClick={onClick}>Search</button>
      </div>
      {
        listOfCitiesData.map((cityWeatherData, index) => (
          <div
            className="removable-forecast"
            key={`${cityWeatherData.name}-forecast-weather`}
            data-testid={`${cityWeatherData.name}-forecast-weather`}>
            <WeatherDisplay
              weatherData={cityWeatherData.daily ? cityWeatherData.daily : []}
              timezone={cityWeatherData.timezone}
              displayName={cityWeatherData.name} />
            <button
              className="remove-button"
              onClick={() => onDeleteClick(index)}
              data-testid={`${cityWeatherData.name}-remove-button`}>X</button>
          </div>
        ))
      }
    </div>
  );
}

export default AssignmentPart2;
