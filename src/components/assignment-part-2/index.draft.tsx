import { render, screen, fireEvent } from '@testing-library/react';
import AssignmentPart2 from './index';

jest.mock('../../hooks', () => ({
    useForecastWeatherDataFromCityName: () => {
        return [{timezone: "America/Los_Angeles", name: "test", daily: [{ weather: [{ icon: "" , description: ""}], temp: {min: 12, max: 13}}]}, () => {}];
}
}));

describe('AssignmentPart2', () => {
    test('should contain WeatherDisplay when there is weather data', () => {
        render(<AssignmentPart2 />);

        const searchInput = screen.getByPlaceholderText('Location to search');
        fireEvent.change(searchInput, {target: {value: 'Coimbra'}})

        const searchButton = screen.getByRole('button');
        fireEvent.click(searchButton);

        expect(screen.getByText('Part 2')).toBeInTheDocument();
        expect(screen.getByTestId('city-search')).toBeInTheDocument();
        expect(screen.getByTestId('Coimbra-forecast-weather')).toBeInTheDocument();
    });
});
