import { useState, useEffect } from 'react';
import { LOCATION_IQ_API_KEY } from "../constants";
import { Location } from './index.d';

export const useReverseGeolocationData = (locationData: Location): Array<string> => {
  const [reverseGeolocationData, setReverseGeolocationData] = useState([]);

  useEffect(() => {
    if (locationData.lat && locationData.long) {
      const url = `https://eu1.locationiq.com/v1/reverse.php?key=${LOCATION_IQ_API_KEY}&lat=${locationData.lat}&lon=${locationData.long}&format=json`;

      fetch(url)
        .then((resp) => resp.json())
        .then((data) => {
            setReverseGeolocationData(data.display_name);
        })
        .catch(() => {
            window.alert("Something went wrong when fetching reverse geolocation data");
        });
    }
  }, [locationData.lat, locationData.long]);

  return [reverseGeolocationData];
}
