import { useState, useEffect } from 'react';
import { OPEN_WEATHER_API_KEY } from "../constants";
import { Location, ForecastWeather } from './index.d';

export const useForecastWeatherDataFromCoords = (locationData: Location) => {
  const [forecastWeatherData, setForecastWeatherData] = useState([]);
  forecastWeatherData as [] | ForecastWeather;
  const [location, setLocation] = useState(locationData);
  const getForecastWeatherData = (newLocation: Location) => {
    setLocation(newLocation);
  }

  useEffect(() => {
    if (location.lat && location.long) {
      const url = `https://api.openweathermap.org/data/2.5/onecall?lat=${location.lat}&lon=${location.long}&units=metric&exclude=current,minutely,hourly,alerts&appid=${OPEN_WEATHER_API_KEY}`;

      fetch(url)
        .then((resp) => resp.json())
        .then((data) => {
          setForecastWeatherData(data);
        })
        .catch(() => {
            setForecastWeatherData([]);
            window.alert("Something went wrong when fetching forecast weather data");
        });
    }
  }, [location]);

  return [forecastWeatherData, getForecastWeatherData];
}
