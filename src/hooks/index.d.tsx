type Weather = {
    description: string;
    icon: string;
};

type WeatherData = {
    dt: number,
    weather: Array<Weather>,
    temp: {
        min: number;
        max: number;
    }
};

export type Location = {
    lat: string;
    long: string;
};

export type ForecastWeather = {
    daily: [] | WeatherData[];
    timezone: string;
};

export type CurrentWeather = {
    name: string;
    timezone: string;
    daily: [] | WeatherData[];
};
