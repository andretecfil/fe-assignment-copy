import { useState, useEffect } from 'react';
import { OPEN_WEATHER_API_KEY } from "../constants";
import { CurrentWeather } from './index.d';

export const useForecastWeatherDataFromCityName = (locationData: string) => {
  const [currentWeatherData, setCurrentWeatherData] = useState({});
  currentWeatherData as CurrentWeather;
  const [location, setLocation] = useState(locationData);
  const getWeatherData = (newLocation) => {
    setLocation(newLocation);
  }

  useEffect(() => {
    if (location?.length > 0) {
      const urlFromCityName = `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${OPEN_WEATHER_API_KEY}`;

      fetch(urlFromCityName, { mode: 'cors' })
        .then((resp) => {
          if(!resp.ok) throw 'An error occurred';
          
          return resp.json();
        })
        .then((data) => {
          const urlFromCoords = `https://api.openweathermap.org/data/2.5/onecall?lat=${data?.coord?.lat}&lon=${data?.coord?.lon}&units=metric&exclude=current,minutely,hourly,alerts&appid=${OPEN_WEATHER_API_KEY}`;
          const cityName = data.name;

          fetch(urlFromCoords, { mode: 'cors' })
            .then((resp) => resp.json())
            .then((data) => {
              setCurrentWeatherData(
                {
                  ...data,
                  name: cityName
                });
            })
            .catch(() => {
              setCurrentWeatherData({});
              window.alert("Something went wrong when fetching forecast weather data");
            })
        })
        .catch(() => {
          setCurrentWeatherData({});
          window.alert("Something went wrong when fetching current weather data");
        });
    }
  }, [location]);

  return [currentWeatherData, getWeatherData];
}
