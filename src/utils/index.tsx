import { DEFAULT_LOCATION } from "../constants";

export const getLocation = () => {
    let location = DEFAULT_LOCATION;

    navigator.geolocation?.getCurrentPosition((position) => {
        location.lat = `${position.coords.latitude}`;
        location.long = `${position.coords.longitude}`;
    });

    return location;
};
