import AssignmentPart1 from './components/assignment-part-1';
import AssignmentPart2 from './components/assignment-part-2';
import './App.css';

function App() {
  return (
    <div className="App">
      <AssignmentPart1 />
      <AssignmentPart2 />
    </div>
  );
}

export default App;
