const { wait } = require("@testing-library/react");

describe('App usage test', () => {
    it('should ', () => {
      cy.viewport(2440, 2440);

      cy.visit('localhost:3000');

      cy.get('[data-testid="assignement-part-1"]').contains("Part 1");
      cy.get('[data-testid="weather-display"]').should('have.length', 1);

      cy.get('[data-testid="assignement-part-2"]').contains("Part 2");
      cy.get('.search-button').contains("Search");

      cy.get('[data-testid="city-search"]').get('input').type('Porto');
      cy.get('.search-button').click();
      cy.wait(1000);
      cy.get('[data-testid="weather-display"]').should('have.length', 2);

      cy.get('[data-testid="city-search"]').get('input').type('Coimbra');
      cy.get('.search-button').click();
      cy.wait(1000);
      cy.get('[data-testid="weather-display"]').should('have.length', 3);

      cy.get('[data-testid="city-search"]').get('input').type('Leiria');
      cy.get('.search-button').click();
      cy.wait(1000);
      cy.get('[data-testid="weather-display"]').should('have.length', 4);

      // delete an item and check if not exists after that
      cy.get('[data-testid="Coimbra-remove-button').click();
      cy.wait(1000);
      cy.get('[data-testid="weather-display"]').should('have.length', 3);
      cy.get('[data-testid="Coimbra-forecast-weather').should('not.exist');

      // try to add a forecast for an existing city
      cy.get('[data-testid="city-search"]').get('input').type('Porto');
      cy.get('.search-button').click();
      cy.wait(1000);
      cy.on('window:alert', (str) => {
        expect(str).to.equal('There is a weather forecast for that city already');
      });

      // error search case
      /*cy.get('[data-testid="city-search"]').get('input').type('[]asd]');
      cy.get('.search-button').click();
      cy.wait(1000);
      cy.on('window:alert', (str) => {
        expect(str).to.equal('Something went wrong when fetching current weather data');
      });*/
    });
});
